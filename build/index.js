const ora = require('ora');
const path = require('path');
const chalk = require('chalk');
const webpack = require('webpack');
const express = require('express');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const historyApiFallback = require('connect-history-api-fallback');

const { 
  NODE_ENV = 'production', 
  PORT = 3000,
} = process.env;

const webpackConfig = (
  NODE_ENV === 'development'
  ? require('./webpack.dev.conf')
  : require('./webpack.prod.conf')
);

if (NODE_ENV === 'development') {
  const app = express();
  const compiler = webpack(webpackConfig);

  const devMiddleware = webpackDevMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath,
    quiet: true,
  });

  const hotMiddleware = webpackHotMiddleware(compiler, {
    log: () => {},
  });

  compiler.plugin('compilation', function (compilation) {
    compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
      hotMiddleware.publish({ action: 'reload' });
      cb();
    });
  });

  app.use(historyApiFallback());
  app.use(devMiddleware);
  app.use(hotMiddleware);

  const uri = 'http://localhost:' + 3000;

  devMiddleware.waitUntilValid(function () {
    console.log('> Listening at ' + uri + '\n');
  });

  app.listen(PORT, function (err) {
    if (err) {
      console.log(err);
      return;
    }
  });
} else {
  const spinner = ora('building for production...');
  spinner.start();

  webpack(webpackConfig, function (err, stats) {
    spinner.stop();
    if (err) throw err;
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false,
    }) + '\n\n');

    console.log(chalk.cyan('  Build complete.\n'));
  });
}