/* eslint-disable */
import React from 'react';
import { createDevTools } from 'redux-devtools';
import Inspector from 'redux-devtools-inspector';
import DockMonitor from 'redux-devtools-dock-monitor';

const DevTools = createDevTools(
  <DockMonitor
    toggleVisibilityKey="ctrl-h"
    changePositionKey="ctrl-q"
    fluid={ false }
    defaultSize={ 350 }
    defaultIsVisible={ false }
  >
    <Inspector theme="monokai" isLightTheme={ false } />
  </DockMonitor>,
);

export default DevTools;
