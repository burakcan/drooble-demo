import 'regenerator-runtime/runtime';
import 'normalize.css';
import 'mdi/css/materialdesignicons.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { browserHistory, Router, Route } from 'react-router';
import configureStore from './configureStore';
import './global.scss';
import Main from './Main';
import Detail from './Detail';

const { NODE_ENV } = process.env;

let DevTools = null;
if (NODE_ENV === 'development') {
  DevTools = require('./DevTools'); // eslint-disable-line
}

const sagas = [
  Main.saga,
  Detail.saga,
];

const reducer = combineReducers({
  main: Main.reducer,
  detail: Detail.reducer,
});

const store = configureStore({ reducer, sagas, DevTools });

ReactDOM.render(
  <div>
    <Provider store={ store }>
      <Router history={ browserHistory }>
        <Route path="/" component={ Main.Container }>
          { Detail.routes }
        </Route>
      </Router>
    </Provider>
    { NODE_ENV === 'development' && <DevTools store={ store } /> }
  </div>
  ,
  document.getElementById('app'),
);
