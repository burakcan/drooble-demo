import React, { PropTypes } from 'react';
import Spinner from '../Spinner';
import styles from './style.scss';

export default function YTEmbed({ videoID }) {
  return (
    <div className={ styles.YTEmbed }>
      { !videoID ? (
        <Spinner />
      ) : (
        <iframe
          id="ytplayer"
          type="text/html"
          width="640"
          height="360"
          src={ `http://www.youtube.com/embed/${videoID}?autoplay=1&origin=http://example.com` }
          frameBorder="0"
        />
      ) }
    </div>
  );
}

YTEmbed.defaultProps = {
  videoID: null,
};

YTEmbed.propTypes = {
  videoID: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.oneOf([null]),
  ]),
};
