import React, { PropTypes } from 'react';
import classnames from 'classnames';
import styles from './style.scss';

export default function Button({ type, icon, children, className, onClick }) {
  return (
    <button
      onClick={ onClick }
      className={
        classnames({
          [styles.Button]: true,
          [styles[type]]: true,
        }, className)
      }
    >
      { icon && (
        <i className={ `mdi mdi-${icon} mdi-18px` } />
      )}
      <span>{ children }</span>
    </button>
  );
}

Button.defaultProps = {
  children: '',
  type: 'default',
  icon: null,
  className: '',
  onClick: () => {},
};

Button.propTypes = {
  children: PropTypes.node,
  type: PropTypes.oneOf(['default', 'danger', 'clear', 'clear-blue']),
  icon: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
};
