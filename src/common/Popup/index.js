import React, { PropTypes } from 'react';
import styles from './style.scss';

export default function Popup({ children, onClose }) {
  return (
    <div className={ styles.Overlay }>
      <div className={ styles.Popup }>
        <button className={ styles.Close } onClick={ onClose }>
          <i className="mdi mdi-close" />
        </button>
        <div className={ styles.ScrollContainer }>
          { children }
        </div>
      </div>
    </div>
  );
}

Popup.defaultProps = {
  onClose: () => {},
};

Popup.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func,
};
