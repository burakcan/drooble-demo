import reducer from './reducer';
import routes from './routes';
import saga from './saga';
import Container from './Container';

export default {
  reducer,
  routes,
  saga,
  Container,
};
