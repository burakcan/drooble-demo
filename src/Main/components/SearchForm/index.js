import React, { PropTypes } from 'react';
import classnames from 'classnames';
import Spinner from '../../../common/Spinner';
import styles from './style.scss';

export default function SearchForm({ onSubmit, onChange, busy }) {
  const className = classnames({
    [styles.Form]: true,
    [styles.busy]: busy,
  });

  return (
    <form
      onSubmit={ (event) => {
        event.preventDefault();
        onSubmit();
      }}
      className={ className }
    >
      <input
        required
        onChange={ onChange }
        type="text"
        placeholder="Enter a Youtube URL"
      />
      <button type="submit">
        { busy ? (
          <Spinner />
        ) : (
          <i className="mdi mdi-chevron-right mdi-36px" />
        ) }
      </button>
    </form>
  );
}

SearchForm.defaultProps = {
  onSubmit: () => {},
  onChange: () => {},
  busy: false,
};

SearchForm.propTypes = {
  onSubmit: PropTypes.func,
  onChange: PropTypes.func,
  busy: PropTypes.bool,
};
