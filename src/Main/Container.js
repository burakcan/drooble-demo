import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import SearchForm from './components/SearchForm';
import {
  createSearchValueChange,
  createSubmitSearch,
} from './actions';
import { getBusy } from './reducer';

class MainContainer extends Component {
  constructor(props, context) {
    super(props, context);

    this.onSearchValueChange = this.onSearchValueChange.bind(this);
  }

  onSearchValueChange(event) {
    this.props.changeSearchValue(
      event.target.value,
    );
  }

  render() {
    return (
      <div className="MainContainer">
        <SearchForm
          onSubmit={ this.props.submitSearch }
          onChange={ this.onSearchValueChange }
          busy={ this.props.searchBusy }
        />
        { this.props.children }
      </div>
    );
  }
}

MainContainer.defaultProps = {
  children: null,
};

MainContainer.propTypes = {
  children: PropTypes.element,
  changeSearchValue: PropTypes.func.isRequired,
  submitSearch: PropTypes.func.isRequired,
  searchBusy: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    searchBusy: getBusy(state),
  };
}

const mapDispatchToProps = {
  changeSearchValue: createSearchValueChange,
  submitSearch: createSubmitSearch,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainContainer);
