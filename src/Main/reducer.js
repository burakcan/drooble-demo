import { createSelector } from 'reselect';
import { SEARCH_VALUE_CHANGE, SUBMIT_SEARCH, SEARCH_ENDED } from './actions';

const initialState = {
  searchValue: '',
  busy: false,
};

export default function mainReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SEARCH_VALUE_CHANGE:
      return {
        ...state,
        searchValue: payload,
      };

    case SUBMIT_SEARCH:
      return {
        ...state,
        busy: true,
      };

    case SEARCH_ENDED:
      return {
        ...state,
        busy: false,
      };

    default:
      return state;
  }
}

function getBranch(state) {
  return state.main;
}

export const getSearchValue = createSelector(
  getBranch,
  branch => branch.searchValue,
);

export const getBusy = createSelector(
  getBranch,
  branch => branch.busy,
);
