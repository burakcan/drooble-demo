import React from 'react';
import { Route } from 'react-router';
import Container from './Container';

export default [
  <Route key="main" path="/" component={ Container } />,
];
