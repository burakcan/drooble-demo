import { browserHistory } from 'react-router';
import { spawn, take, select, call, put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { alert } from 'alertify.js';
import { SUBMIT_SEARCH, createSearchEnded } from './actions';
import { getSearchValue } from './reducer';

function getVideoID(url) {
  const match = url.match(
    /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/ // eslint-disable-line
  );

  return (match && match[7].length === 11) ? match[7] : false;
}

function* watchSearchSubmit() {
  while (yield take(SUBMIT_SEARCH)) {
    const value = yield select(getSearchValue);
    const videoID = yield call(getVideoID, value);

    yield delay(1500); // eyecandy :)

    if (!videoID) {
      yield call(alert, 'The URL is not valid. Please enter a valid Youtube video url.');
    } else {
      yield call(browserHistory.push, `/detail/${videoID}`);
    }

    yield put(
      createSearchEnded(),
    );
  }
}

export default function* mainSaga() {
  yield spawn(watchSearchSubmit);
}
