export const SEARCH_VALUE_CHANGE = 'MAIN_SEARCH_VALUE_CHANGE';
export const SUBMIT_SEARCH = 'MAIN_SUBMIT_SEARCH';
export const SEARCH_ENDED = 'MAIN_SEARCH_ENDED';

export function createSearchValueChange(value) {
  return {
    type: SEARCH_VALUE_CHANGE,
    payload: value,
  };
}

export function createSubmitSearch() {
  return {
    type: SUBMIT_SEARCH,
    payload: null,
  };
}

export function createSearchEnded() {
  return {
    type: SEARCH_ENDED,
    payload: null,
  };
}
