import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

export default function configureStore({ reducer, initialState = {}, sagas = [] }) {
  const sagaMiddleware = createSagaMiddleware();

  const store = {
    ...createStore(
      reducer,
      initialState,
      applyMiddleware(sagaMiddleware),
    ),
    runSaga: sagaMiddleware.run,
  };

  setImmediate(
    () => sagas.forEach(saga => store.runSaga(saga)),
  );

  return store;
}
