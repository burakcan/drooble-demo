import { compose, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, autoRehydrate } from 'redux-persist';
import immutableStateInvariant from 'redux-immutable-state-invariant';

export default function configureStore({ reducer, initialState = {}, sagas = [], DevTools }) {
  const sagaMiddleware = createSagaMiddleware();

  const store = {
    ...createStore(
      reducer,
      initialState,
      compose(
        applyMiddleware(
          immutableStateInvariant(),
          sagaMiddleware,
        ),
        DevTools.instrument(),
        autoRehydrate(),
      ),
    ),
    runSaga: sagaMiddleware.run,
  };

  setImmediate(
    () => sagas.forEach(saga => store.runSaga(saga)),
  );

  persistStore(store);
  return store;
}
