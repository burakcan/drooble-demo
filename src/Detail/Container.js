import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Popup from '../common/Popup';
import YTEmbed from '../common/YTEmbed';
import Button from '../common/Button';
import CommentBox from './components/CommentBox';
import CommentItem from './components/CommentItem';
import styles from './style.scss';
import { createAddComment } from './actions';
import { getComments } from './reducer';

class DetailContainer extends Component {
  renderComments(tree, parentID = null) {
    return (
      Object
      .values(tree)
      .filter(comment => parentID === comment.parentID)
      .map(comment =>
        <CommentItem
          { ...comment }
          key={ comment.ID }
          onReply={ this.props.addComment }
        >
          { this.renderComments(tree, comment.ID) }
        </CommentItem>,
      )
    );
  }

  render() {
    return (
      <div>
        <Popup onClose={ browserHistory.goBack }>
          <YTEmbed videoID={ this.props.routeParams.videoID } />
          <div className={ styles.ButtonBar }>
            <Button type="clear-blue" icon="heart-outline" className={ styles.fl }>Like</Button>
            <Button type="clear-blue" icon="share-variant" className={ styles.fl }>Share</Button>
            <Button type="danger" className={ styles.fr }>Delete</Button>
            <Button className={ styles.fr }>Edit</Button>
          </div>
          <div className={ styles.CommentBox }>
            <CommentBox
              onSubmit={ this.props.addComment }
              videoID={ this.props.routeParams.videoID }
            />
          </div>
          { Object.keys(this.props.comments).length ? (
            <div className={ styles.Comments }>
              { this.renderComments(this.props.comments) }
            </div>
          ) : null }
        </Popup>
      </div>
    );
  }
}

DetailContainer.defaultProps = {
  routeParams: {
    videoID: null,
  },
  comments: {},
};

DetailContainer.propTypes = {
  routeParams: PropTypes.shape({
    videoID: PropTypes.string,
  }),
  addComment: PropTypes.func.isRequired,
  comments: PropTypes.objectOf(
    PropTypes.shape({
      parentID: PropTypes.string,
      ID: PropTypes.string,
      content: PropTypes.string,
      timeStamp: PropTypes.number,
    }),
  ),
};

function mapStateToProps(state, { params }) {
  return {
    comments: getComments(state, params.videoID),
  };
}

const mapDispatchToProps = {
  addComment: createAddComment,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DetailContainer);
