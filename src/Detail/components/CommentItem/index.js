import React, { PropTypes, Component } from 'react';
import Button from '../../../common/Button';
import CommentBox from '../CommentBox';
import styles from './style.scss';

export default class CommentItem extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      commentBoxOpen: false,
    };

    this.toggleCommentBox = this.toggleCommentBox.bind(this);
    this.onReply = this.onReply.bind(this);
  }

  onReply(content, parentID, videoID) {
    this.toggleCommentBox();
    this.props.onReply(content, parentID, videoID);
  }

  toggleCommentBox() {
    this.setState({
      commentBoxOpen: !this.state.commentBoxOpen,
    });
  }

  render() {
    const { content, children, ID, timeStamp, videoID } = this.props;
    const [time, date] = (() => {
      const $date = new Date(timeStamp);
      return [
        $date.toTimeString(),
        $date.toDateString(),
      ];
    })();

    return (
      <div className={ styles.Comment }>
        <div className={ styles.Profile }>
          <div
            className={ styles.Avatar }
            style={{
              backgroundImage: `url(https://api.adorable.io/avatars/252/${ID})`,
            }}
          />
          <a href="?" className={ styles.Name }>Stoyan Daskaloff</a>
          <div className={ styles.DateTime }>{ `${date}, AT ${time}` }</div>
        </div>
        <div className={ styles.Content }>
          <p>{ content }</p>
          <div className={ styles.ButtonBar }>
            <Button type="clear-blue" icon="heart-outline" className={ styles.fl }>Like</Button>
            <Button type="clear-blue" icon="share-variant" className={ styles.fl }>Share</Button>
            <Button
              onClick={ this.toggleCommentBox }
              type="clear-blue"
              icon="comment-text-outline"
              className={ styles.fl }
            >Comment</Button>
            <Button type="clear" icon="flag-outline" className={ styles.fr }>Report</Button>
          </div>
        </div>
        { children.length ? (
          <div>
            { children }
          </div>
        ) : null }
        { this.state.commentBoxOpen ? (
          <div className={ styles.CommentBox }>
            <CommentBox
              videoID={ videoID }
              onSubmit={ this.onReply }
              parentID={ ID }
            />
          </div>
        ) : null }
      </div>
    );
  }
}

CommentItem.defaultProps = {
  children: [],
};

CommentItem.propTypes = {
  children: PropTypes.node,
  ID: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  onReply: PropTypes.func.isRequired,
  timeStamp: PropTypes.number.isRequired,
  videoID: PropTypes.string.isRequired,
};
