import React, { PropTypes, Component } from 'react';
import styles from './style.scss';

export default class CommentBox extends Component {
  constructor(props, context) {
    super(props, context);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();

    const value = this.input.value;
    const { videoID, parentID } = this.props;

    this.props.onSubmit(value, parentID, videoID);

    this.input.value = '';
  }

  render() {
    return (
      <form
        onSubmit={ this.onSubmit }
        className={ styles.CommentBox }
      >
        <input
          ref={ (domEl) => { this.input = domEl; }}
          placeholder="comment..."
        />
      </form>
    );
  }
}

CommentBox.defaultProps = {
  parentID: null,
  onSubmit: () => {},
};

CommentBox.propTypes = {
  onSubmit: PropTypes.func,
  parentID: PropTypes.oneOfType([
    PropTypes.oneOf([null]),
    PropTypes.string,
  ]),
  videoID: PropTypes.string.isRequired,
};
