import uid from 'uid';
import { REHYDRATE } from 'redux-persist/constants';
import { createSelector } from 'reselect';
import { ADD_COMMENT } from './actions';

const initialState = {
  comments: {},
};

export default function detailReducer(state = initialState, { type, payload }) {
  switch (type) {
    case REHYDRATE: {
      const persisted = payload.detail;

      if (persisted) {
        return persisted;
      }

      return state;
    }

    case ADD_COMMENT: {
      const ID = uid();

      return {
        ...state,
        comments: {
          ...state.comments,
          [payload.videoID]: {
            ...state.comments[payload.videoID],
            [ID]: { ...payload, ID },
          },
        },
      };
    }

    default:
      return state;
  }
}

export const getComments = createSelector(
  (state, videoID) => state.detail.comments[videoID],
  comments => comments,
);
