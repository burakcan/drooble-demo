import reducer from './reducer';
import saga from './saga';
import routes from './routes';

export default {
  reducer,
  saga,
  routes,
};
