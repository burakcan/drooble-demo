export const ADD_COMMENT = 'DETAIL_ADD_COMMENT';

export function createAddComment(content, parentID, videoID) {
  return {
    type: ADD_COMMENT,
    payload: {
      content,
      parentID,
      videoID,
      timeStamp: Date.now(),
    },
  };
}
