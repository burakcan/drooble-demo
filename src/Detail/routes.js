import React from 'react';
import { Route } from 'react-router';
import Container from './Container';

export default [
  <Route key="detail" path="detail/:videoID" component={ Container } />,
];
