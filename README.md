# Youtube Video Popup Demo for Drooble 🎶
Demo: http://drooble-demo.surge.sh

## Running and building locally
You need to have Nodejs and Yarn installed on your machine.
Alternatively, you can use NPM but it is much slower :)

Install dependencies 🤞:

    $ yarn


Run development server 🤘:

    $ yarn run dev


Build for production 👌:

    $ yarn run build


## Notes
  - I don't have a Photoshop license, so design is slighly different in some places. :)
  - Cheers. 🍺
